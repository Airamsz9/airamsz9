# 1. Windows

Vamos a hacer una tarea programada y otra diferida con Windows

## 1.1 Tarea diferida

Vamos a programar una tarea diferida para
que nos muestre un mensaje en pantalla.
En Windows 7 para abrir el programador de
tareas hacemos Panel de control ->
Herramientas administrativas ->
Programador de tareas.

![Fotos](./programado/1-programador.png)

![Fotos](./programado/2-creartarea.png)

![Fotos](./programado/3-diferida.png)

![Fotos](./programado/4-hora.png)

![Fotos](./programado/5-mostrarmensaje.png)

![Fotos](./programado/6-resumen.png)

![Fotos](./programado/7-mensaje.png)

## 1.2 Tarea periódica

Vamos a programar una tarea periódica para apagar el equipo.
El comando para apagar el sistema es shutdown. Para ello, usamos los argumentos -s -t
y el tiempo en segundos.

  ![Fotos](./programado/8-apagado.png)

  ![Fotos](./programado/9-diariamente.png)

  ![Fotos](./programado/10-horaapagada.png)

  ![Fotos](./programado/programado.png)

  ![Fotos](./programado/apagadominuto.png)

# 2. SO GNU/Linux


## 2.1 Tarea diferida


    El servicio atd es el responsable de la ejecución de los comandos at. Para
    asegurarnos de que esté en ejecución:

      Yast -> Servicios.
          systemctl status atd

![Fotos](./programado/13-atdlinux.png)

Mi script lo que hace, es copiar a través de ssh a otra máquina/servidor. Para ello,
hacemos uso del comando scp. Recordemos, que para que funcione, tienes que pasar los certificados para evitar que pregunte la contraseña. Quedaría algo así:


![Fotos](./programado/14-script.png)

Ejecutamos el comando, para que haga la acción a las 12:38 mediante el script que hemos creado.

![Fotos](./programado/15-at.png)

Comprobamos que en el servidor se ha creado la carpeta proyecto, creando así un efecto "dropbox".

![Fotos](./programado/15-directorio.png)


## 2.2 Tarea periódica


      Programar una tarea periódica (crontab) para crear un directorio cada mes.
      Para definir una tarea ASINCRONA ponemos el script de ejecución en alguno de los directorios siguientes:

          /etc/cron.hourly, cada hora
          /etc/cron.daily, diariamente
          /etc/cron.weekly, semanalmente
          /etc/cron.monthly, mensualmente

      En mi caso sería la mensual, debido a que quiero que me cree una carpeta con el nombre en el mes en el que estamos.

  El script que he creado es:

![Fotos](./programado/16-mensual.png)

Lo movemos a /etc/cron.monthly por querer hacerlo cada mes.

![Fotos](./programado/17-mover.png)

Y comprobamos como dentro de la carpeta "proyecto" se ha creado la carpeta con el mes en el que estamos. En combinación con la otra, podríamos tener una copia de seguridad cada mes en nuestro servidor de manera automatizada.

![Fotos](./programado/17-carpetacreada.png)
