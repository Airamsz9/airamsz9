#!/usr/bin/ruby
#PROGRAMA REALIZADO POR AIRAM SUÁREZ ZAMORA 2ºASIR
if ARGV.size != 1
  puts "Usar el programa <install-and-remove> con 1  argumento"
  puts "software-list.txt"
  exit 1
end 

comando = `whoami`
user = comando.chop

if user != "root"
puts "ERROR: Ejecutando script sin permisos."
puts ""
exit 1
end

filename = ARGV[0]
content = `cat #{filename}`
lines   = content.split("\n")

lines.each do |row|
campos = row.split(":")
check = `whereis #{campos[0]} |grep bin |wc -l`
program_check = check.to_i

if campos[1]=="status" or campos[1]=="s" then
	if program_check == 0 then 
		puts "El #{campos[0]} no instalado"
	elsif program_check == 1 then 
		puts "El #{campos[0]} ya instalado"
	end

elsif campos[1]=="install" or campos[1]=="i" then
	if program_check == 0 then
  		system("apt-get install #{campos[0]}")
  	else 
  		puts"El #{campos[0]} ya instalado"
	end

elsif campos[1]=="remove" or campos[1]=="r" then
  	if program_check == 1 then
  		system("apt-get remove #{campos[0]}")
	else 
		puts "El #{campos[0]} borrado"
	end

end

end