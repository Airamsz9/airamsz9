#!/usr/bin/ruby
#PROGRAMA REALIZADO POR AIRAM SUÁREZ ZAMORA 2ºASIR
if ARGV.size != 1
  puts "Usar el programa <createusers> con 1 argumento"
  puts "userlist.txt"
  exit 1
end

comando = `whoami`
user = comando.chop

if user != "root"
puts "ERROR: Ejecutando script sin permisos."
puts ""
exit 1
end

filename = ARGV[0]
content = `cat #{filename}`
lines   = content.split("\n")

lines.each do |row|
  puts "Procesando: #{row}"

  system("useradd -d /home/#{row} -m #{row}")

  system("mkdir /home/#{row}/private")
        system("chmod 700 /home/#{row}/private")
  system("mkdir /home/#{row}/group")
        system("chmod 750 /home/#{row}/group")
  system("mkdir /home/#{row}/public")
        system("chmod 755 /home/#{row}/public")

end
