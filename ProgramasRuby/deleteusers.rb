#!/usr/bin/ruby

if ARGV.size != 1
  puts "Usar el programa <deleteusers> con 1 argumento"
  puts "  filename : Nombre de fichero"
  exit 1
end

filename = ARGV[0]
content = `cat #{filename}`
lines   = content.split("\n")

lines.each do |row|
  puts "Procesando: #{row}"

  system("userdel #{filename}")

end
