#Servidor LDAP - OpenSUSE


##Enlaces de interés sobre teoría LDAP:

###1. Servidor LDAP

Comenzamos la instalación y configuración del servidor LDAP.

####1.1 Preparar la máquina

Vamos a usar una MV OpenSUSE 13.2 para montar nuestro servidor LDAP con:
Configuración MV
Nombre equipo: ldap-serverXX
Además en /etc/hosts añadiremos:
127.0.0.2   ldap-serverXX.curso1617   ldap-serverXX
127.0.0.3   nombrealumnoXX.curso1617  nombrealumnoXX

![hosts](./sis/01.png)


####1.2 Instalación del Servidor LDAP

Procedemos a la instalación del módulo Yast que sirve para gestionar el servidor LDAP (yast2-auth-server).
Hacemos lo siguiente:

Ir a Yast -> Servidor de autenticación. Aparecerá como Authentication Server.

![autenticación](./sis/02.1.yast2.png)

Instalar los paquetes openldap2, krb5-server y krb5-client -> Aceptar

![instalar](./sis/02.authentication.png)

Iniciar servidor LDAP -> Sí

![instalar](./sis/04.png)

Registrar dameon SLP -> No
Puerto abierto en el cortafuegos -> Sí -> Siguiente
Tipo de servidor -> autónomo -> Siguiente

![instalar](./sis/05.png)

Configuración TLS -> NO habilitar -> Siguiente

![instalar](./sis/06.png)

Tipo de BD -> hdb
DN base -> dc=nombredealumnoXX,dc=curso1617. Donde XX es el número del puesto de cada uno.
DN administrador -> dn=Administrator
Añadir DN base -> Sí
Contraseña del administrador
Directorio de BD -> /var/lib/ldap

![instalar](./sis/07.png)

Usar esta BD predeterminada para clientes LDAP -> Sí -> Siguiente
Habilitar kerberos -> No

![instalar](./sis/08.png)

Veamos ejemplo de la configuración final:

![instalar](./sis/09.png)

Comprobaciones

systemctl status slapd, para comprobar el estado del servicio.

![instalar](./sis/10.png)

systemctl enable slapd, para activar el servicio automáticamente al reiniciar la máquina.

![instalar](./sis/11.png)

slapcat para comprobar que la base de datos está bien configurada.

![instalar](./sis/12.png)

Podemos comprobar el contenido de la base de datos LDAP usando la herramienta gq. Esta herramienta es un browser LDAP.
Comprobar que tenemos creadas las unidades organizativas: groups y people.

![instalar](./sis/13.png)

###1.3 Problemas

Si tenemos que desinstalar el paquete hacemos:

zypper remove yast2-auth-server
zypper remove openldap2 krb5-server krb5-client
mv /etc/openldap /etc/openldap.000
mv /var/lib/ldap /var/lib/ldap.000


###1.4 Crear usuarios y grupos LDAP

Yast -> Usuarios Grupos -> Filtro -> LDAP.
Crear los grupos piratas (Estos se crearán dentro de la ou=groups).
Crear los usuarios pirata21, pirata21 (Estos se crearán dentro de la ou=people).

![instalar](./sis/14.png)

![instalar](./sis/15.png)

##2. Autenticación

En este punto vamos a escribir información en el servidor LDAP.

2.1 Preparativos

Vamos a otra MV OpenSUSE 13.2.
Cliente LDAP con OpenSUSE 13.2:
Configuración MV
Nombre equipo: ldap-clientXX
Dominio: curso1617
Asegurarse que tenemos definido en el fichero /etc/hosts del cliente, el nombre DNS con su IP correspondiente:
127.0.0.2         ldap-clientXX.curso1617   ldap-clientXX
ip-del-servidor   ldap-serverXX.curso1617   ldap-serverXX   nombredealumnoXX.curso1617   nombrealumnoXX

![instalar](./sis/16.png)

Usar gq en el cliente para comprobar que se han creado bien los usuarios.
File -> Preferencias -> Servidor -> Nuevo
URI = ldap://ldap-serverXX
Base DN = dc=vargasXX,dc=curso1617

![instalar](./sis/17.png)

###2.2 Instalar cliente LDAP

Debemos instalar el paquete yast2-auth-client, que nos ayudará a configurar la máquina para autenticación. En Yast aparecerá como Authentication Client.
Configuración de la conexión

Yast -> Authentication client

![instalar](./sis/20.png)

Under Basic Settings click on sssd. A new dialogue box will appear, in that write LDAP under domain section. Click OK & Close the dialogue box.
Under Configured Authentication Domains list, you can see domain/LDAP. Click Edit
id_provider = ldap
auth_provider = ldap
chpass_provider = ldap
ldap_schema = rfc2307bis . ldap_uri = ldap://ldapserver.mycompany.in
ldap_search base = dc=example, dc=com

![instalar](./sis/19.png)