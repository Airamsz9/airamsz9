# 1. Servidor Samba
****
## Preparativos

Configurar el servidor GNU/Linux. Usar los siguientes valores:
Nombre de equipo: smb-serverXX (Donde XX es el número del puesto de cada uno).
Añadir en /etc/hosts los equipos smb-cliXXa y smb-cliXXb (Donde XX es el número del puesto de cada uno).

- Cliente A:

![](./smb/nombreclientea.png)

- Server IP y host modificado:

![](./smb/ipserver.png)

![](./smb/smbserverhosts.png)

#1.2 Usuarios locales

Vamos a GNU/Linux, y creamos los siguientes grupos y usuarios:

Crear los grupos jedis, siths y starwars.
Crear el usuario smbguest. Para asegurarnos que nadie puede usar smbguest para entrar en nuestra máquina mediante login, vamos a modificar este usuario y le ponemos como shell /bin/false.
Por entorno gráfico lo cambiamos usando Yast.
Por comandos el cambio se hace editando el fichero /etc/passwd.
Dentro del grupo jedis incluir a los usuarios jedi1, jedi2 y supersamba.
Dentro del grupo siths incluir a los usuarios sith1 y sith2 y supersamba.
Dentro del grupo starwars, poner a todos los usuarios siths, jedis, supersamba y a smbguest.

![](./smb/gruposserver.png)

![](./smb/binfalse.png)

#1.3 Crear las carpetas para los futuros recursos compartidos

Vamos a crear las carpetas ((samba14/corusant.d) (samba14/tatooine.d) (samba14/public.d))

![](./smb/carpetas.png)

de los recursos compartidos con los permisos siguientes:

- /srv/sambaXX/public.d
Usuario propietario supersamba.
Grupo propietario starwars.
Poner permisos 775.

- /srv/sambaXX/corusant.d
Usuario propietario supersamba.
Grupo propietario siths.
Poner permisos 770.

- /srv/sambaXX/tatooine.d
Usuario propietario supersamba.
Grupo propietario jedis.
Poner permisos 770.

![](./smb/propietarios.png)

![](./smb/grupos.png)

![](./smb/chmod.png)

# 1.4 Instalar Samba Server

Vamos a hacer una copia de seguridad del fichero de configuración existente cp /etc/samba/smb.conf /etc/samba/smb.conf.000.
Podemos usar comandos o el entorno gráfico para instalar y configurar el servicio Samba. Como estamos en OpenSUSE vamos a usar Yast.
Yast -> Samba Server
Workgroup: starwars
Sin controlador de dominio.
En la pestaña de Inicio definimos
Iniciar el servicio durante el arranque de la máquina.
Ajustes del cortafuegos -> Abrir puertos

# 1.5 Configurar el servidor Samba

Vamos a configurar los recursos compartido del servidor Samba. Podemos hacerlo modificando el fichero de configuración o por entorno gráfico con Yast.

Capturar imágenes del proceso.
Yast -> Samba Server -> Recursos compartidos

Tenemos que montar una configuración como la siguiente:

Donde pone XX, sustituir por el núméro del puesto de cada uno
public, será un recurso compartido accesible para todos los usuarios en modo lectura.
cdrom, es el recurso dispositivo cdrom de la máquina donde está instalado el servidor samba.

![](./smb/lineasmbconf.png)

![](./smb/testparm.png)

# 1.6 Usuarios Samba

Después de crear los usuarios en el sistema, hay que añadirlos a Samba.

- smbpasswd -a nombreusuario, para crear clave de Samba para un usuario del sistema.

![](./smb/jedisamba.png)

![](./smb/sithsamba.png)

- pdbedit -L, para comprobar la lista de usuarios Samba.

![](./smb/pbdedit.png)

# 1.7 Reiniciar

- Ahora que hemos terminado con el servidor, hay que reiniciar el servicio para que se lean los cambios de configuración.

systemctl stop smb, systemctl start smb, systemctl status smb
systemctl stop nmb, systemctl start nmb, systemctl status nmb

![](./smb/systemctlreiniciar.png)

Capturar imagen de los siguientes comando de comprobación:

    sudo testparm     # Verifica la sintaxis del fichero de configuración del servidor Samba
    sudo netstat -tap # Vemos que el servicio SMB/CIF está a la escucha

![](./smb/sudotestparm.png)

![](./smb/smbnetstat.png)

# 2. Windows (MV3 => smb-cliXXb)

Configurar el cliente Windows.
Usar nombre smb-cliXXb y la IP que hemos establecido.
Configurar el fichero ...\etc\hosts de Windows.
En los clientes Windows el software necesario viene preinstalado.

![](./smb/hostswindows.png)

#2.1 Cliente Windows GUI

Desde un cliente Windows vamos a acceder a los recursos compartidos del servidor Samba.

![](./smb/conectadoalsever.png)

Comprobar los accesos de todas las formas posibles. Como si fuéramos:
un sith
un jedi
y/o un invitado.

![](./smb/acceso.png)

![](./smb/corusantwindows.png)

Después de cada conexión se quedan guardada la información en el cliente Windows (Ver comando net use).
Para cerrar las conexión SMB/CIFS que ha realizado el cliente al servidor, usamos el comando: C:>net use * /d /y.

![](./smb/netuse.png)

Capturar imagen de los siguientes comandos para comprobar los resultados:
smbstatus, desde el servidor Samba.

![](./smb/smbstatus.png)

netstat -ntap, desde el servidor Samba.

![](./smb/netstat-ntap.png)

netstat -n, desde el cliente Windows.

![](./smb/netstat-n.png)

# 2.2 Cliente Windows comandos

En el cliente Windows, para consultar todas las conexiones/recursos conectados hacemos C:>net use.
Si hubiera alguna conexión abierta la cerramos.
C:>net use * /d /y, para cerrar las conexiones SMB.
net use ahora vemos que NO hay conexiones establecidas.

![](./smb/cerrarconexiones.png)

Abrir una shell de windows. Usar el comando net use /?, para consultar la ayuda del comando.
Con el comando net view, vemos las máquinas (con recursos CIFS) accesibles por la red.
Vamos a conectarnos desde la máquina Windows al servidor Samba usando los comandos net.

![](./smb/netview.png)

# 2.3 Montaje automático

El comando net use S: \\ip-servidor-samba\panaderos /USER:pan1 establece una conexión del rescurso panaderos y lo monta en la unidad S.
Ahora podemos entrar en la unidad S ("s:") y crear carpetas, etc.

![](./smb/comandonetuse.png)

Capturar imagen de los siguientes comandos para comprobar los resultados:

smbstatus, desde el servidor Samba.

![](./smb/smbstatusnetuse.png)

netstat -ntap, desde el servidor Samba.

![](./smb/netstatnetuse.png)


netstat -n, desde el cliente Windows.

![](./smb/netstatn.png)

# 3.1 Cliente GNU/Linux GUI

Desde en entorno gráfico, podemos comprobar el acceso a recursos compartidos SMB/CIFS.

Estas son algunas herramientas:

Yast en OpenSUSE
Konqueror en KDE

![](./smb/konqueror.png)

Ejemplo accediendo al recurso prueba del servidor Samba, pulsamos CTRL+L y escribimos smb://ip-del-servidor-samba:

linux-gui-client

![](./smb/smbclia.png)


En el momento de autenticarse para acceder al recurso remoto, poner en Dominio el nombre-netbios-del-servidor-samba.
Capturar imagen de lo siguiente:

Probar a crear carpetas/archivos en corusant y en tatooine.
Comprobar que el recurso public es de sólo lectura.

![](./smb/accederclia.png)

![](./smb/corusantclia.png)

smbstatus, desde el servidor Samba.

![](./smb/smbstatusclia.png)

netstat -ntap, desde el servidor Samba.

![](./smb/netstatclia.png)

netstat -n, desde el cliente.

![](./smb/netstatnclia.png)

#3.2 Cliente GNU/Linux comandos

Capturar imagenes de todo el proceso.

Primero comprobar el uso de las siguientes herramientas:
sudo smbtree                       # Muestra todos los equipos/recursos de la red SMB/CIFS
smbclient --list ip-servidor-samba # Muestra los recursos SMB/CIFS de un equipo concreto

![](./smb/smbtreeclia.png)

![](./smb/smbtree2.png)

![](./smb/smbclientlistclia.png)


Ahora crearemos en local la carpeta /mnt/sambaXX-remoto/corusant.
MONTAJE: Con el usuario root, usamos el siguiente comando para montar un recurso compartido de Samba Server, como si fuera una carpeta más de nuestro sistema: mount -t cifs //172.18.XX.55/corusant /mnt/sambaXX-remoto/corusant -o username=sith1

![](./smb/mkdircorusantclia.png)

![](./smb/mountcorusant.png)

COMPROBAR: Ejecutar el comando df -hT. Veremos que el recurso ha sido montado.
samba-linux-mount-cifs

![](./smb/dfht.png)

Si montamos la carpeta de corusat, lo que escribamos en /mnt/sambaXX-remoto/corusant debe aparecer en la máquina del servidor Samba. ¡Comprobarlo!

![](./smb/archivocorusant.png)

![](./smb/comprobacion.png)

Para desmontar el recurso remoto usamos el comando umount.

smbstatus, desde el servidor Samba.

![](./smb/smbstatusmontaje.png)

netstat -ntap, desde el servidor Samba.

![](./smb/netstatmontaje.png)

netstat -n, desde el cliente Windows.

![](./smb/netstatnmontaje.png)

# 3.3 Montaje automático

Acabamos de acceder a los recursos remotos, realizando un montaje de forma manual (comandos mount/umount). Si reiniciamos el equipo cliente, podremos ver que los montajes realizados de forma manual ya no están (df -hT). Si queremos volver a acceder a los recursos remotos debemos repetir el proceso de montaje manual, a no ser que hagamos una configuración de montaje permanente o automática.

Para configurar acciones de montaje automáticos cada vez que se inicie el equipo, debemos configurar el fichero /etc/fstab. Veamos un ejemplo:
//smb-serverXX/public /mnt/sambaXX-remoto/public cifs username=sith1,password=clave 0 0

![](./smb/fstab.png)

# 4. Preguntas para resolver

- ¿Las claves de los usuarios en GNU/Linux deben ser las mismas que las que usa Samba?

	No tienen porque ser las mismas.

- ¿Puedo definir un usuario en Samba llamado sith3, y que no exista como usuario del sistema?

	No se puede. 

- ¿Cómo podemos hacer que los usuarios sith1 y sith2 no puedan acceder al sistema pero sí al samba? (Consultar /etc/passwd)

	Ir a /etc/passwd y poner los usuarios sith1 y sith2 en false.

- Añadir el recurso [homes] al fichero smb.conf según los apuntes. ¿Qué efecto tiene?

	Aparece oculta.







