###Airam Suárez Zamora	

#Instalación del servicio NFS

Vamos a la MV con Windows 2008 Server

1. Agregar rol Servidor de Archivos.
2. Marcar Servicio para NFS.

![](./david/01.png)

![](./david/02.png)

##Configurar el servidor NFS

Crear la carpeta c:\exportXX\public y c:\exportXX\private.

![](./david/03.png)

Picar en la carpeta public, botón derecho propiedades -> Compartir NFS, y configurarla para que sea accesible desde la red en modo lectura/escritura con NFS.

![](./david/05.png)

Picar en la carpeta private, botón derecho propiedades -> Compartir NFS, y configurarla para que sea accesible desde la red sólo en modo sólo lectura.

![](./david/04.png)

#Cliente NFS

Aquí los hacemos con Open Suse en vez de Windows 7 por no disponer la ISO. Aquí la configuración del cliente NFS:

![](./david/07.png)

![](./david/06.png)

Hacemos ping al servidor desde el cliente.

![](./david/08.png)

Usamos el comando nmap

![](./david/09.png)

Para consultar los recursos que exporta el servidor: showmount -e 172.18.14.21

![](./david/10.png)

mount o df -hT, para comprobar los recursos montados en nuestras carpetas locales.

![](./david/12.png)

# Servidor NFS

Instalar servidor NFS por Yast.

![](./david/16.png)

Crear las siguientes carpetas/permisos:
/srv/exportXX/public, usuario y grupo propietario nobody:nogroup
/srv/exportXX/private, usuario y grupo propietario nobody:nogroup 770

![](./david/15.png)

Vamos configurar el servidor NFS de la siguiente forma:
La carpeta /srv/exportXX/public, será accesible desde toda la red en modo lectura/escritura.

![](./david/17.png)

La carpeta /srv/exportXX/private, sea accesible sólo desde la IP del cliente, sólo en modo lectura.

![](./david/18.png)

Showmount -e localhost. Muestra la lista de recursos exportados por el servidor NFS.

![](./david/19.png)


#Cliente NFS Suse a Suse

Hacemos ping al servidor Open Suse desde el cliente.

![](./david/20.png)

Showmount -e ip-del-servidor: Muestra la lista de recursos exportados por el servidor NFS.

![](./david/21.png)

Montar y usar cada recurso compartido desde el cliente

Estamos en el equipo cliente.
Crear las carpetas:
/mnt/remotoXX/public
/mnt/remotoXX/private
showmount -e IP-DEL-SERVIDOR, para consultar los recursos que exporta el servidor.
Montar cada recurso compartido en su directorio local correspondiente

![](./david/23.png)

mount o df -hT, para comprobar los recursos montados en nuestras carpetas locales.

![](./david/24.png)

#Montaje automático

Configurar montaje autoḿatico del recurso compartido public.
Usar Yast -> particionador -> NFS -> Add.
Modificar directamente en el fichero /etc/fstab

![](./david/25.png)

![](./david/26.png)

Incluir contenido del fichero /etc/fstab en la entrega.

![](./david/28.png)