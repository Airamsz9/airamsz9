# 1. Introducción

Es muy común que nos encontremos desarrollando una aplicación y llegue el momento que decidamos tomar todos sus archivos y migrarlos ya sea al ambiente de producción, de prueba o simplemente probar su comportamiento en diferentes plataformas y servicios. Para situaciones de este estilo existen herramientas que, entre otras cosas, nos facilitan el embalaje y despliegue de la aplicación, es aquí donde entra en juego Docker.

Esta herramienta nos permite crear lo que ellos denominan contenedores, lo cual son aplicaciones empaquetadas auto-suficientes, muy livianas que son capaces de funcionar en prácticamente cualquier ambiente, ya que tiene su propio sistema de archivos, librerías, terminal, etc.
# 2. Requisitos

Vamos a usar MV OpenSUSE. Nos aseguraremos que tiene una versión del Kernel 3.10 o superior (uname -a).
# 3. Instalación y primeras pruebas

    Ejecutar como superusuario:

zypper in docker              
![Init](./docker/1.png)
systemctl start docker       
![Init](./docker/2.png)                          
docker version  
![Init](./docker/3.png)              
usermod -a -G docker USERNAME

![Init](./docker/4.png)

    Salir de la sesión y volver a entrar con nuestro usuario.
    Ejecutar con nuestro usuario para comprobar que todo funciona:

docker images

docker ps -a

docker run hello-world

![Init](./docker/5.png)

docker images

![Init](./docker/6.png)


docker ps -a

![Init](./docker/7.png)

# 4. Configuración de la red

Habilitar el acceso a la red externa a los contenedores

Si queremos que nuestro contenedor tenga acceso a la red exterior, debemos activar la opción IP_FORWARD (net.ipv4.ip_forward). Lo podemos hacer en YAST.

![Init](./docker/8.png)

Reiniciar el equipo para que se apliquen los cambios.

# 5. Crear un contenedor manualmente

Nuestro SO base es OpenSUSE, pero vamos a crear un contenedor Debian8, y dentro instalaremos Nginx.

## 5.1 Crear una imagen

    Enlace de interés: Cómo instalar y usar docker

docker images

docker search debian  


![Init](./docker/9.png)


docker pull debian:8   

![Init](./docker/10.png)

docker pull opensuse

![Init](./docker/12.png)

docker ps -a      

![Init](./docker/15.png)

docker ps              

![Init](./docker/16.png)

    Vamos a crear un contenedor con nombre mv_debian a partir de la imagen debian:8, y ejecutaremos /bin/bash:

docker run --name=mv_debian -i -t debian:8 /bin/bash

![Init](./docker/17.png)



root@IDContenedor:/# cat /etc/motd   

![Init](./docker/18.png)

root@IDContenedor:/# apt-get update

root@IDContenedor:/# apt-get install -y nginx

![Init](./docker/19.png)

root@IDContenedor:/# apt-get install -y vim  

![Init](./docker/20.png)

root@IDContenedor:/# /usr/sbin/nginx

root@IDContenedor:/# ps -ef

![Init](./docker/21.png)

    Creamos un fichero HTML (holamundo.html).

root@IDContenedor:/# "<p>Hola Nombre-del-alumno!</p>" > /var/www/html/holamundo.html

![Init](./docker/22.png)

    Creamos tambien un script /root/server.sh con el siguiente contenido:

    #!/bin/bash

    echo "Booting nginx..."
    /usr/sbin/nginx &

    echo "Waiting..."
    while(true) do
      sleep 60
    done

![Init](./docker/24.png)

david@camaleon:~/devops> docker ps

![Init](./docker/25.png)

    Ahora con esto podemos crear la nueva imagen a partir de los cambios que realizamos sobre la imagen base:

docker commit * airam/nginx

![Init](./docker/26.png)

docker images

![Init](./docker/27.png)

docker ps

docker stop mv_debian

docker ps

![Init](./docker/28.png)

docker ps -a      

![Init](./docker/30.png)

docker rm IDcontenedor

docker ps -a

![Init](./docker/31.png)

## 5.2 Crear contenedor

Bien, tenemos una imagen con Nginx instalado, probemos ahora la magia de Docker.

    Iniciemos el contenedor de la siguiente manera:

docker run --name=mv_nginx -p 80 -t dvarrui/nginx /root/server.sh

![Init](./docker/32.png)

docker ps

![Init](./docker/33.png)

Ahora en una nueva ventana ejecutaremos docker ps. Podemos apreciar que la última columna nos indica que el puerto 80 del contenedor está redireccionado a un puerto local 0.0.0.0.:NNNNNN->80/tcp, vayamos al explorador y veamos si conectamo con Nginx dentro del contenedor.

![Init](./docker/34.png)

    Paramos el contenedor y lo eliminamos.

docker ps

![Init](./docker/33.png)

docker stop mv_nginx

docker ps -a

docker rm mv_nginx

![Init](./docker/35.png)

docker ps -a

![Init](./docker/36.png)


## 5.3 Más comandos

Información sobre otros comandos útiles:

    docker start CONTAINERID, inicia un contenedor que estaba parado.
    docker attach CONTAINERID, conecta el terminal actual con el interior de contenedor.

# 6. Crear un contenedor con Dockerfile

Ahora vamos a conseguir el mismo resultado del apartado anterior, pero usando un fichero de configuración, llamado Dockerfile

## 6.1 Comprobaciones iniciales:


## 6.2 Preparar ficheros

    Crear directorio /home/alumno/docker, poner dentro los siguientes ficheros.
    Dockerfile:

![Init](./docker/37.png)

![Init](./docker/38.png)

    Necesitaremos también los ficheros server.sh y holamundo.html que vimos antes.

![Init](./docker/39.png)

![Init](./docker/41.png)

## 6.3 Crear imagen

El fichero Dockerfile contiene la información necesaria para contruir el contenedor, veamos:


![Init](./docker/42.png)


## 6.4 Crear contenedor y comprobar

Desde otra terminal hacer docker..., para averiguar el puerto de escucha del servidor Nginx.
  Comprobar en el navegador URL: http://localhost:PORTNUMBER

![Init](./docker/43.png)
