# 1. Introducción

Según Wikipedia:

    Vagrant es una herramienta para la creación y configuración de entornos
    de desarrollo virtualizados.

    Originalmente se desarrolló para VirtualBox y sistemas de configuración tales
    como Chef, Salt y Puppet. Sin embargo desde la versión 1.1 Vagrant es
    capaz de trabajar con múltiples proveedores, como VMware, Amazon EC2, LXC,
    DigitalOcean, etc.2

    Aunque Vagrant se ha desarrollado en Ruby se puede usar en multitud de
    proyectos escritos en otros lenguajes.

# 2. Primeros pasos

##2.1 Instalar

La instalación debemos hacerla en una máquina real.

Hay varias formas de instalar Vagrant:
apt-get install vagrant o
Usando un paquete Vagrant-deb Disponible para descargar del servidor Leela.


##2.2. Proyecto

Crear un directorio para nuestro proyecto vagrant (Donde XX es el número de cada alumno):
    mkdir mivagrantXX
    cd mivagrantXX
    vagrant init
    ![Init](./vagrant/1.png)

## 2.3 Imagen, caja o box

Ahora necesitamos obtener una imagen(caja, box) de un sistema operativo. Vamos, por ejemplo, a conseguir una imagen de un Ubuntu Precise de 32 bits:
vagrant box add micajaXX_ubuntu_precise32 http://files.vagrantup.com/precise32.box

![Init](./vagrant/2.png)

![Init](./vagrant/3.png)

vagrant box list: Lista las cajas/imágenes disponibles actualmente en nuestra máquina.
Para usar una caja determinada en nuestro proyecto, modificamos el fichero Vagrantfile (dentro de la carpeta de nuestro proyecto).
Cambiamos la línea config.vm.box = "base" por config.vm.box = "micajaXX_ubuntu_precise32".

![Init](./vagrant/4.png)


## 2.4 Iniciar una nueva máquina

Vamos a iniciar una máquina virtual nueva usando Vagrant:

cd mivagrantXX
vagrant up: comando para iniciar una nueva instancia de la máquina.

![Init](./vagrant/6.png)

vagrant ssh: Conectar/entrar en nuestra máquina virtual usando SSH.

![Init](./vagrant/7.png)

#3. Configuración del entorno virtual

##3.1 Carpetas sincronizadas

La carpeta del proyecto que contiene el Vagrantfile es visible para el sistema el virtualizado, esto nos permite compartir archivos fácilmente entre los dos entornos.
Para identificar las carpetas compartidas dentro del entorno virtual, hacemos:
vagrant up
vagrant ssh
ls /vagrant

![Init](./vagrant/8.png)

Esto nos mostrará que efectivamente el directorio /vagrant dentro del entorno virtual posee el mismo Vagrantfile que se encuentra en nuestro sistema anfitrión.
##3.2 Redireccionamiento de los puertos

Cuando trabajamos con máquinas virtuales, es frecuente usarlas para proyectos enfocados a la web, y para acceder a las páginas es necesario configurar el enrutamiento de puertos.

Entramos en la MV e instalamos apache.
vagrant ssh
apt-get install apache2

![Init](./vagrant/13.png)

Modificar el fichero Vagrantfile, de modo que el puerto 4567 del sistema anfitrión sea enrutado al puerto 80 del ambiente virtualizado.
config.vm.network :forwarded_port, host: 4567, guest: 80

![Init](./vagrant/9.png)

Luego iniciamos la MV (si ya se encuentra en ejecución lo podemos refrescar con vagrant reload)
Para confirmar que hay un servicio a la escucha en 4567, desde la máquina real podemos ejecutar los siguientes comandos:
nmap -p 4500-4600 localhost

![Init](./vagrant/11.png)

netstat -ntap

![Init](./vagrant/12.png)

En la máquina real, abrimos el navegador web con el URL http://127.0.0.1:4567. En realidad estamos accediendo al puerto 80 de nuestro sistema virtualizado.

![Init](./vagrant/14.png)

#4. Ejemplos de configuración Vagrantfile

A continuación se muestran ejemplos de configuración Vagrantfile que NO ES NECESARIO hacer. Sólo es información.

Enlace de interés Tutorial Vagrant. ¿Qué es y cómo usarlo?
Ejemplo para configurar la red:

config.vm.network "private_network", ip: "192.168.33.10"
Ejemplo para configurar las carpetas compartidas:

config.vm.synced_folder "htdocs", "/var/www/html"
Ejemplo para configurar la conexión SSH de vagrant a nuestra máquina virtual:

config.ssh.username = 'root'
config.ssh.password = 'vagrant'
config.ssh.insert_key = 'true'
Ejemplo para configurar la ejecución remota de aplicaciones gráficas instaladas en la máquina virtual, mediante SSH:

config.ssh.forward_agent = true
config.ssh.forward_x11 = true
#5.Suministro

Una de los mejores aspectos de Vagrant es el uso de herramientas de suministro. Esto es, ejecutar "una receta" o una serie de scripts durante el proceso de arranque del entorno virtual para instalar, configurar y personalizar un sin fin de aspectos del SO del sistema anfitrión.

vagrant halt, apagamos la MV.
vagrant destroy y la destruimos para volver a empezar.
##5.1 Suministro mediante shell script

Ahora vamos a suministrar a la MV un pequeño script para instalar Apache.

Crear el script install_apache.sh, dentro del proyecto con el siguiente contenido:

![Init](./vagrant/15.png)

Poner permisos de ejecución al script.
Vamos a indicar a Vagrant que debe ejecutar dentro del entorno virtual un archivo install_apache.sh.

Modificar Vagrantfile y agregar la siguiente línea a la configuración: config.vm.provision :shell, :path => "install_apache.sh"

![Init](./vagrant/16.png)

Si usamos los siguiente config.vm.provision "shell", inline: '"echo "Hola"', ejecuta directamente el comando especificado.
Volvemos a crear la MV.
Podemos usar vagrant reload si está en ejecución para que coja el cambio de la configuración.
Podremos notar, al iniciar la máquina, que en los mensajes de salida se muestran mensajes que indican cómo se va instalando el paquete de Apache que indicamos.

![Init](./vagrant/17.png)


Para verificar que efectivamente el servidor Apache ha sido instalado e iniciado, abrimos navegador en la máquina real con URL http://127.0.0.1:4567.

![Init](./vagrant/18.png)

##5.2 Suministro mediante Puppet

Enlace de interés:

Crear un entorno de desarrollo con vagrant y puppet
friendsofvagrant.github.io -> Puppet Provisioning
Veamos imágenes de ejemplo (de Aarón Gonźalez Díaz) con Vagrantfile configurado con puppet:

vagranfile-puppet

Fichero de configuración de puppet mipuppet.pp:

vagran-puppet-pp-file
Se pide hacer lo siguiente.

Modificar el archivo el archivo Vagrantfile de la siguiente forma:

![Init](./vagrant/20.png)

Crear un fichero manifests/default.pp, con las órdenes/instrucciones puppet para instalar el programa nmap. Ejemplo:
package { 'nmap':
  ensure => 'present',
}
Para que se apliquen los cambios de configuración, tenemos dos formas:

(A) Parar la MV, destruirla y crearla de nuevo (vagrant halt, vagrant destroy y vagrant up).
(B) Con la MV encendida recargar la configuración y volver a ejecutar la provisión (vagrant reload, vagrant provision).

![Init](./vagrant/21.png)

#6. Nuestra caja personalizada

En los apartados anteriores hemos descargado una caja/box de un repositorio de Internet, y luego la hemos provisionado para personalizarla. En este apartado vamos a crear nuestra propia caja/box personalizada a partir de una MV de VirtualBox.

##6.1 Preparar la MV VirtualBox

Lo primero que tenemos que hacer es preparar nuestra máquina virtual con una configuración por defecto, por si queremos publicar nuestro Box, ésto se realiza para seguir un estándar y que todo el mundo pueda usar dicho Box.

Crear una MV VirtualBox nueva o usar una que ya tengamos.
Instalar OpenSSH Server.
Indicaciones de ¿Cómo crear una Base Box en Vagrant a partir de una máquina virtual para preparar la MV de VirtualBox.
Crear el usuario Vagrant, para poder acceder a la máquina virtual por SSH. A este usuario le agregamos una clave pública para autorizar el acceso sin clave desde Vagrant.
useradd -m vagrant
su - vagrant
mkdir .ssh
wget https://raw.githubusercontent.com/mitchellh/vagrant/master/keys/vagrant.pub -O .ssh/authorized_keys
chmod 700 .ssh
chmod 600 .ssh/authorized_keys

![Init](./vagrant/26+.png)

Poner clave vagrant al usuario vagrant y al usuario root.
Tenemos que conceder permisos al usuario vagrant para que pueda configurar la red, instalar software, montar carpetas compartidas, etc. para ello debemos configurar /etc/sudoers (visudo) para que no nos solicite la password de root, cuando realicemos estas operación con el usuario vagrant.

Añadir vagrant ALL=(ALL) NOPASSWD: ALL a /etc/sudoers.

![Init](./vagrant/29.png)

##6.2 Crear la caja vagrant

Una vez hemos preparado la máquina virtual ya podemos crear el box.

Vamos a crear una nueva carpeta mivagrantXXconmicaja, para este nuevo proyecto vagrant.
Ejecutamos vagrant init para crear el fichero de configuración nuevo.
Localizar el nombre de nuestra máquina VirtualBox (Por ejemplo, v1-opensuse132-xfce).
Crear la caja package.box a partir de la MV.
vagrant-package

![Init](./vagrant/26.png)

![Init](./vagrant/27.png)

Comprobamos que se ha creado la caja package.box en el directorio donde hemos ejecutado el comando.
vagrant-package_box_file

![Init](./vagrant/28.png)

Hacemos ssh a la máquina.

![Init](./vagrant/30.png)
