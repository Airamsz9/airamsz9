#Acceso remoto SSH

***- Airam Suárez Zamora	2º ASIR***

##1. Preparativos
###1.1 Servidor SSH

- **Configurar el servidor GNU/Linux con siguientes valores:**    
 - -SO GNU/Linux: OpenSUSE    
 - -IP estática: 172.18.XX.31    
 - -Nombre de equipo: ssh-serverXX    
 - -Configuración de las MV's    
 - -Añadir en /etc/hosts los equipos ssh-clientXXa y ssh-clientXXb (Donde XX es el puesto del alumno


![show] (./ssh/nombre.png)

![ip] (./ssh/ipserver.png)

![host] (./ssh/hostserver.png)

   
 - Crear los siguientes usuarios en ssh-server14:   
  - suarez1  
  - suarez2   
  - suarez3  
  - suarez4  


![users] (./ssh/usuarios.png)


###1.2 Cliente GNU/Linux

- **Configurar el cliente 1 GNU/Linux con siguientes valores:**    
 - -SO GNU/Linux: OpenSUSE    
 - -IP estática: 172.18.XX.32    
 - -Nombre de equipo: ssh-clientXXa    
 - -Configuración de las MV's    
 - -Añadir en /etc/hosts los equipos ssh-serverXX y ssh-clientXXb (Donde XX es el puesto del alumno
 
 
![users] (./ssh/ipclientesuse.png)

![host] (./ssh/hostsclientesuse.png)

###1.3 Cliente Windows
- **Instalar software cliente SSH en Windows. Para este ejemplo usaremos PuTTY.**

![putty] (./ssh/putty.png)

- **Configurar el cliente 1 GNU/Linux con siguientes valores:**    
 - -SO Windows 7    
 - -IP estática: 172.18.XX.11    
 - -Nombre de equipo: ssh-clientXXb    
 - -Configuración de las MV's    
 - -Añadir en /etc/hosts los equipos ssh-serverXX y ssh-clientXXa (Donde XX es el puesto del alumno
 
 
![equipo] (./ssh/windowsequipo.png)

![host] (./ssh/ip7.png)

**Ping de Server a los dos clientes:**

![ping] (./ssh/pingambas.png)

**Ping del cliente *A* los otros:**

![ping] (./ssh/pingambos.png)

**Ping del cliente *B* a los otros:**

![ping] (./ssh/pingwin.png)


##2. Instalación del servicio SSH


###2.1 Comprobación

Desde el propio ssh-server, verificar que el servicio está en ejecución.

![ping] (./ssh/systemctl.png)

###2.2 Primera conexión SSH desde ssh-clientXXa

- Comprobamos la conectividad con el servidor desde el cliente con ping ssh-server.
- Desde el cliente comprobamos que el servicio SSH es visible con **nmap ssh-server**. Debe mostrarnos que el puerto 22 está abierto. Esto es, debe aparecer una línea como **"22/tcp open ssh"**.

![open] (./ssh/sshnmap.png)

- Vamos a comprobar el funcionamiento de la conexión SSH desde cada cliente
- Desde el ssh-client1 nos conectamos mediante **ssh airam@ssh-server**. Capturar imagen del intercambio de claves que se produce en el primer proceso de conexión SSH.


![open] (./ssh/primeraconx.png)

- Comprobar contenido del fichero **$HOME/.ssh/known_hosts** en el equipo ssh-client A. 


![open] (./ssh/llssh.png)


###2.3 ¿Y si cambiamos las claves del servidor?

- Confirmar que existen los siguientes ficheros en **/etc/ssh**, Los ficheros ssh_host*key y ssh_host*key.pub, son ficheros de clave pública/privada que identifican a nuestro servidor frente a nuestros clientes.

- Modificar el fichero de configuración SSH **(/etc/ssh/sshd_config)** para dejar una única línea: HostKey **/etc/ssh/ssh_host_rsa_key**. Comentar el resto de líneas con configuración HostKey. Este parámetro define los ficheros de clave publica/privada que van a identificar a nuestro servidor. Con este cambio decimos que sólo vamos a usar las claves del tipo RSA.

![open] (./ssh/sshdmodificado.png)

**Regenerar certificados**. Vamos a cambiar o volver a generar nuevas claves públicas/privadas para la identificación de nuestro servidor.

- En ssh-server, como usuario root ejecutamos: ssh-keygen -t rsa -f /etc/ssh/ssh_host_rsa_key.
- No poner password al certificado de la máquina.
- Reiniciar el servicio SSH: systemctl restart sshd.
- Comprobar que el servicio está en ejecución correctamente: systemctl status sshd

![open] (./ssh/sshkeygen.png)

![open] (./ssh/restartsshd.png)

## 3. Personalización del prompt Bash

- Por ejemplo, podemos añadir las siguientes líneas al fichero de configuración del usuario1 en la máquina servidor 

![open] (./ssh/bashrc.png)

- Además, crear el fichero /home/1er-apellido-alumno1/.alias con el siguiente contenido:

![open] (./ssh/alias.png)

## 4. Autenticación mediante claves públicas

- Iniciamos sesión con nuestro usuario nombre-alumno de la máquina **ssh-clientXXa**.
- Ejecutamos **ssh-keygen -t rsa** para generar un nuevo par de claves para el usuario en **/home/nuestro-usuario/.ssh/id_rsa y /home/nuestro-usuario/.ssh/id_rsa.pub.**
- Ahora vamos a copiar la clave pública (**id_rsa.pub**) del usuario (nombre-de-alumno) de la máquina cliente, al fichero **"authorized_keys"** del usuario remoto 1er-apellido-alumno4 (que está definido en el servidor. Hay dos formas de hacerlo:
    - El modo recomendado es usando el comando **ssh-copy-id**. Ejemplo para copiar la clave pública del usuario actual al usuario remoto en la máquina remota: **ssh-copy-id usuario-remoto@hostname-remoto**.


![open] (./ssh/sshkeygencliente.png)


![open] (./ssh/sshcopy.png)

- Comprobar que ahora al acceder remotamente vía SSH
    - Desde ssh-clientXXa, NO se pide password.


![open] (./ssh/comprobacionclienteA.png)

## 5. Uso de SSH como túnel para X

- Instalar en el servidor una aplicación de entorno gráfico (APP1) que no esté en los clientes. Por ejemplo Geany. Si estuviera en el cliente entonces buscar otra aplicación o desinstalarla en el cliente.
- Modificar servidor SSH para permitir la ejecución de aplicaciones gráficas, desde los clientes. Consultar fichero de configuración /etc/ssh/sshd_config (Opción X11Forwarding yes)
- Comprobar funcionamiento de APP1 desde cliente1. Por ejemplo, con el comando ssh -X remoteuser1@ssh-server, podemos conectarnos de forma remota al servidor, y ahora ejecutamos APP1 de forma remota.

![open] (./ssh/borrando.png)

![open] (./ssh/x11forwarding.png)

## 6. Aplicaciones Windows nativas

- Podemos tener aplicaciones Windows nativas instaladas en ssh-server mediante el emulador WINE.

- Instalar emulador Wine en el ssh-server.
- Ahora podríamos instalar alguna aplicación (APP2) de Windows en el servidor SSH usando el emulador Wine. O podemos usar el Block de Notas que viene con Wine: wine notepad.
- Comprobar el funcionamiento de APP2 en ssh-server.
- Comprobar funcionamiento de APP2, accediendo desde ssh-client1.
- En este caso hemos conseguido implementar una solución similar a RemoteApps usando SSH.

![open] (./ssh/notepad.png)

## 7. Restricciones de uso

Vamos a modificar los usuarios del servidor SSH para añadir algunas restricciones de uso del servicio.

### 7.1 Sin restricción (tipo 1)

Usuario sin restricciones.

### 7.2 Restricción total (tipo 2)

Vamos a crear una restricción de uso del SSH para un usuario:

- En el servidor tenemos el usuario remoteuser2. Desde local en el servidor podemos usar sin problemas el usuario.
Vamos a modificar SSH de modo que al usar el usuario por ssh desde los clientes tendremos permiso denegado.
Capturar imagen de los siguientes pasos:

- Consultar/modificar fichero de configuración del servidor SSH (/etc/ssh/sshd_config) para restringir el acceso a determinados usuarios. Consultar las opciones AllowUsers, DenyUsers. Más información en: man sshd_config y en el Anexo de este enunciado.

![open] (./ssh/restriccionsuarez2.png)

### 7.4 Restricción sobre aplicaciones (tipo 4)

- Vamos a crear una restricción de permisos sobre determinadas aplicaciones.

    - Usaremos el usuario remoteuser4
    - Crear grupo remoteapps
    - Incluir al usuario en el grupo.
    - Localizar el programa APP1. Posiblemente tenga permisos 755.
    - Poner al programa APP1 el grupo propietario a remoteapps
    - Poner los permisos del ejecutable de APP1 a 750. Para impedir que los que no pertenezcan al grupo puedan ejecutar el programa.
 
 
![open] (./ssh/grupo74.png)

![open] (./ssh/chmod750.png)




















