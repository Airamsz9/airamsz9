#LTSP TRABAJO
##Airam Suárez Zamora 2ºASIR

En primer lugar hacemos un ip a route -n para ver la ruta ip.

![ip a route] (./David/ip.png)

Comprobamos el hostname, uname -a, blkid.

![hostname] (./David/hostname.png)

![uname -a] (./David/uname.png)

![blkid] (./David/blkid.png)

Ahora creamos los 3 usuarios que nos pone la práctica, para a posteriori, usarlo en el cliente.

![usuarios] (./David/3.png)

Después de crear los 3 usuarios, instalamos el openssh y paquetes adicionales

![openssh] (./David/openssh.png)
![standalone] (./David/standalone.png)

Configuramos una doble red en la cual, una sea interna >(clase c) otra en adaptador puente >(clase b)

![doble red] (./David/doble.png)

Ahora, activamos el ltsp para que haga la imagen

![ltsp] (./David/ltsp.png)

Y comprobamos la información de las imagenes

![info] (./David/info.png)

Después de todo esto, miramos el estado del DHCP

![status] (./David/status.png)

Una vez realizado todo esto, preparamos la máquina virtual para que coja la imágen. Para ello, ponemos que inicie en primer lugar desde la red.
 
Ahora, dejo un vídeo para comprobar si el trabajo ha sido realizado correctamente: 

[Url youtube](https://www.youtube.com/watch?v=d-Km3w0HQlA)
