#VNC TRABAJO
##Airam Suárez Zamora 2ºASIR

En primer lugar activamos el ssh.

![ssh open suse] (./vnc/1.png)

Comprobamos el hostname y la ip de cada sistema.

Windows 7

![conf7] (./vnc/conf7.png)

Windows Server

![server] (./vnc/confserver.png)

Open Suse Cliente

![susecl] (./vnc/confsusecliente.png)

Open Suse Servidor

![suseser] (./vnc/confsuseserver.png)

Ahora en Windows 7, activamos el servidor telnet en Windows 7.

![telnet7] (./vnc/telnetw7.png)

Después de activarlo, hay que configurarlo.

![conftelnet] (./vnc/conftelnet.png)

Ahora creamos un usuario "sysadmingame" y lo añadimos en "administradores" y "TelnetClients"

![tclient] (./vnc/telnetclient.png)

Después en el Window Server, agregamos en "roles y características" el servidor telnet.

![tserver] (./vnc/telnetserver.png)

Y comprobamos que hay conexión

![conex] (./vnc/telnet.png)

Ahora instalamos y configuramos el TightVnc en Server y Windows 7 y ponemos el la contraseñas.

![typical] (./vnc/typicalvnc.png)
![passwd] (./vnc/passwd.png)
![7tightvnc] (./vnc/7tightvnc.png)
![newip] (./vnc/newiptightconf.png)
![confser] (./vnc/tightconfserver.png)

Una vez realizado todo esto, comprobamos que se conectan.

![comprobacion] (./vnc/comprobacionvnc.png)

Ahora hacemos lo propio en Open Suse, desde Yast instalamos VNC y le damos a permitir conexión remota.

![vnc] (./vnc/vncsuse.png)

Probamos ahora las distintas combinaciones entre sistemas operativos.

Open Suse a Open Suse 

![status] (./vnc/suseasuse.png)

Open Suse a Server

![status] (./vnc/suseaserver.png)

Windows 7 a Open Suse
 
![status] (./vnc/7asuse.png)

Windows 7 a Server 

![status] (./vnc/comprobacionvnc.png)





